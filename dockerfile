FROM odoo:16

USER root

COPY ./config /etc/odoo

COPY ./base_addons /mnt/extra-addons/base_addons

COPY ./custom_addons /mnt/extra-addons/custom_addons

EXPOSE 8069